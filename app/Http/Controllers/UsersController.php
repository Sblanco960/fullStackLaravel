<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UsersController extends Controller
{
    
    public function index (){        
        return User::all();
    }

    public function update (Request $request){         
        $user = User::find($request['id']);
        $user['name']=$request->name;
        $user['email']=$request->email;
        $user->save();
        return 200;
    }


}
